%MATLAB2020b cript
%name: task_1
%author: Daniel "cynddan" Cyndecki
%date: 13.11.2020
%version: v1.0.1


PLANCK_CONST = 6.62607015e-34;
DIRAC_CONST = PLANCK_CONST/2*pi

sin(pi/(4*exp(1)))

hex2dec('0x0098d6') / (1.445e+23)

(exp(1)-pi)^(1/2)

myPi = pi*10^12;
piString = num2str(myPi);
out = str2double(piString(13))


date = datetime('today');
bday = datetime(2020, 01, 18);
duration = date - bday

R = 6.3781e6;
atan((exp((7^(1/2)/2)-log(R/10^6)))/hex2dec('0xaaff'))

Na = 6.02214076e23;
n = 0.25e-6;
N = Na*n

Nc13 = (((2/7)*(1/101)*N)*1000)/N