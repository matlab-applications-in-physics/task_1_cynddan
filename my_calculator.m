%MATLAB2020b script
%name: my_calculator
%author: Daniel "cynddan" Cyndecki
%date: 13.11.2020
%version: v1.2.0

%define of Planck constant in joules times second
PLANCK_CONST = 6.62607015e-34;
%calculation of reduced Planck constant
zad1 = PLANCK_CONST/(2*pi);
disp('PLANCK_CONST = 6.62607015e-34')
disp(['zad1 = PLANCK_CONST/(2*pi) = ', num2str(zad1)])

%calculation of sinus 45 degrees divided by e
zad2 = sind(45/exp(1));
disp(['zad2 = sind(45/exp(1)) = ', num2str(zad2)])

%covertion of hexadecimal number and calculation equation
zad3 = hex2dec('0x0098d6') / (1.445e+23);
disp(['zad3 = hex2dec("0x0098d6") / (1.445e+23) = ', num2str(zad3)])

%calculation square root of e minus pi
zad4 = sqrt(exp(1)-pi);
disp(['zad4 = sqrt(exp(1)-pi) = ', num2str(zad4)])

%transformations to obtain the 12th decimal place of pi 
myPi = pi*10^12;
piString = num2str(myPi);
zad5 = str2double(piString(13));
disp('myPi = pi*10^12; piString = num2str(myPi)')
disp(['zad5 = str2double(piString(13)) = ', num2str(zad5)])

%creation of variable containing current time
date = datetime('now');
%creation of variable containing time of my last birthday
bday = datetime(1999, 01, 18, 07, 50, 00);
%calculation of passed time since my last birthday
zad6 = date - bday;
disp('date = datetime("now"); bday = datetime(1999, 01, 18, 07, 50, 00)')
disp('zad6 = date - bday = ')
disp(cellstr(zad6))

%declaration of mean Earth radius in meters
R = 6.371e6;
%calculation
zad7 = atan((exp((7^(1/2)/2)-log(R/10^6)))/hex2dec('0xaaff'));
disp('R = 6.371e6')
disp(['zad7 = atan((exp((7^(1/2)/2)-log(R/10^6)))/hex2dec("0xaaff")) = ', num2str(zad7)])

%define Avogadro constant in 1/mol
AVOGADRO_CONSTANT = 6.02214076e23;
%amount of ethanol in mols
n = 0.25e-6;
%number of atoms in quarter micromol of ethanol
zad8 = AVOGADRO_CONSTANT*n*9;
disp('AVOGADRO_CONSTANT = 6.02214076e23; n = 0.25e-6')
disp(['zad8 = AVOGADRO_CONSTANT*n*9 = ', num2str(zad8)])

%calculation of number coal C13 in quarter micromol of ethanol per mil
zad9 = (((2/9)*(1/101)*zad8)*1000)/zad8;
disp(['zad9 = (((2/9)*(1/101)*zad8)*1000‰)/zad8 = ', num2str(zad9),'‰'])